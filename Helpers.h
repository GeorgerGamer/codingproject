#include <iostream>
#include <math.h>
#pragma once
using namespace std;

int n[2]{};

void inputNum()
{
	cout << "Enter first number: ";
	cin >> n[0];

	cout << "Enter second number: ";
	cin >> n[1];
}

void calcNum()
{
	cout << endl;

	int sum = n[0] + n[1];
	double squareRoot = sqrt(sum);

	cout << "Number is " << squareRoot << endl;
}
